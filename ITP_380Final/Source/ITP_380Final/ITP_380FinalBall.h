// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "ITP_380FinalBall.generated.h"

UCLASS(config = Game)
class AITP_380FinalBall : public ACharacter
{
	GENERATED_BODY()

	/** StaticMesh used for the ball */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Ball, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* Ball;

	/** Spring arm for positioning the camera above the ball */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Ball, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArm;

	/** Camera to view the ball */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Ball, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera;
	FVector prevPosition;

public:
	AITP_380FinalBall();

	/** Vertical impulse to apply when pressing jump */
	UPROPERTY(EditAnywhere, Category = Ball)
		float JumpImpulse;

	/** Torque to apply when trying to roll ball */
	UPROPERTY(EditAnywhere, Category = Ball)
		float RollTorque;
    
    UFUNCTION()
    void OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
    
    UFUNCTION()
    void OnOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult);

	/** Indicates whether we can currently jump, use to prevent double jumping */
	bool bCanJump;
	/** Called for side to side input */
	void MoveRight(float Val);

	/** Called to move ball forwards and backwards */
	void MoveForward(float Val);

	void Stop();
protected:

	// AActor interface
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	// End of AActor interface

	virtual void Tick(float DeltaTime) override;

	virtual void BeginPlay() override;


	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface
	FRotator rotation;

    UPROPERTY(EditAnywhere, Category = Rocket)
    TSubclassOf<class ARocket> RocketClass;
    
    UPROPERTY(EditAnywhere, Category = PowerUp)
    TSubclassOf<class APowerUp> PowerUpClass;
    
    UPROPERTY(VisibleAnywhere, Category = "SphereComp")
    class USphereComponent* sphereCollision;

public:
	/** Returns Ball subobject **/
	FORCEINLINE class UStaticMeshComponent* GetBall() const { return Ball; }
	/** Returns SpringArm subobject **/
	FORCEINLINE class USpringArmComponent* GetSpringArm() const { return SpringArm; }
	/** Returns Camera subobject **/
	FORCEINLINE class UCameraComponent* GetCamera() const { return Camera; }

	void OnStartFire();
	void OnStopFire();
	void deacMesh();
	bool isDead();

	void CollectPowerUp(class APowerUp* DamageCauser);
	void LosePowerUp();

	float TakeDamage(float dam, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	void reset();
private:
    class APowerUp* MyPowerUp;
	class ARocket* MyRocket;
	class APowerUpSpawner* MyAPS;
	bool hasPowerUp;
};
