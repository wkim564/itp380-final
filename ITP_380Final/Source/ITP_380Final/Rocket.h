// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Rocket.generated.h"

UCLASS()
class ITP_380FINAL_API ARocket : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARocket();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    USkeletalMeshComponent* GetRocketMesh() {return RocketMesh;}
    
    UAudioComponent* PlayRocketSound(class USoundCue* Sound);
    
    void OnStartFire();
    void OnStopFire();
    
    void WeaponTrace();
    
    void SetMyOwner(class AITP_380FinalBall* own){MyOwner = own;}
    AITP_380FinalBall* GetMyOwner() {return MyOwner;}

protected:
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Rocket")
    USkeletalMeshComponent* RocketMesh;
    
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* FireLoopSound;
    
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* FireFinishSound;
    
    UPROPERTY(Transient)
    class UAudioComponent* FireAC;
    
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    class UParticleSystem* MuzzleFX;
    
    UPROPERTY(Transient)
    class UParticleSystemComponent* FireParticle;
    
    UPROPERTY(EditAnywhere)
    float FireRate;
    
    UPROPERTY(EditAnywhere)
    float WeaponRange;
    
    UPROPERTY(EditDefaultsOnly)
    class UParticleSystem* HitParticle;
    
    UPROPERTY(EditAnywhere, Category = Damage)
    float Damage = 2.0f;
    
    class AITP_380FinalBall* MyOwner;
    FTimerHandle WeaponTimer;
	
};
