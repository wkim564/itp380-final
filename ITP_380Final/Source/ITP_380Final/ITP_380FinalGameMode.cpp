// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "ITP_380Final.h"
#include "ITP_380FinalGameMode.h"
#include "ITP_380FinalBall.h"
#include "MyPlayerController.h"
#include "Ball.h"

AITP_380FinalGameMode::AITP_380FinalGameMode()
{
	PlayerControllerClass = AMyPlayerController::StaticClass();

	// set default pawn class to our ball
	DefaultPawnClass = AITP_380FinalBall::StaticClass();
	time = 150;
	RedScore = 0;
	BlueScore = 0;
	sRedScore = "0";
	sBlueScore = "0";
	
}

void AITP_380FinalGameMode::BeginPlay()
{
	Super::BeginPlay();
	Timer();
	GetRedScore();
	GetBlueScore();
	player2 = UGameplayStatics::CreatePlayer(this->GetWorld());
}

void AITP_380FinalGameMode::Timer()
{
	time--;
	WinTime = 5;
	if (time > 0)
	{
		GetWorldTimerManager().SetTimer(handle, this, &AITP_380FinalGameMode::Timer, 1.0f, false);
		mins = time / 60;
		sMins = FString::FromInt(mins);
		seconds = time % 60;
		if (seconds < 10)
		{
			sSeconds = "0" + FString::FromInt(seconds);
		}
		else
		{
			sSeconds = FString::FromInt(seconds);
		}
	}
	else if(time <= 0)
	{
		GetWorldTimerManager().ClearTimer(handle);
		WinScreen();
	}
}

void AITP_380FinalGameMode::WinScreen()
{
	
	WinTime--;
	if (WinTime > 0)
	{ 
		
		GetWorldTimerManager().SetTimer(handle, this, &AITP_380FinalGameMode::WinScreen, 1.0f, false);
		if (RedScore > BlueScore)
		{
			sSeconds = "Blue Team Wins";
		}
		else if (BlueScore > RedScore)
		{
			sSeconds = "Red Team Wins";
		}
		else
		{
			sMins = "";
			sSeconds = "Draw";
		}
		((ABall*)(ball))->gameOver=true;
	}
	else if (WinTime <= 0)
	{
		GetWorldTimerManager().ClearTimer(handle);
		time = 150;
		BlueScore = 0;
		RedScore = 0;
		ResetWorld();
		Timer();
	}
}

void AITP_380FinalGameMode::ResetWorld(){
	((AITP_380FinalBall*)(player1->GetPawn()))->reset();
	((AITP_380FinalBall*)(player2->GetPawn()))->reset();

	((ABall*)(ball))->SetActorLocation(FVector(-250.0f, 0.0f, 220.0f));
	((ABall*)(ball))->Ball->SetPhysicsLinearVelocity(FVector(0, 0, 0));
	((ABall*)(ball))->gameOver = false;
}

FString AITP_380FinalGameMode::GetBlueScore()
{
	sBlueScore = FString::FromInt(BlueScore);
	return sBlueScore;
}

FString AITP_380FinalGameMode::GetRedScore()
{
	sRedScore = FString::FromInt(RedScore);
	return sRedScore;
}

