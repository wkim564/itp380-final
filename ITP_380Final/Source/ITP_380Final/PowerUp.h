// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "PowerUp.generated.h"

UCLASS()
class ITP_380FINAL_API APowerUp : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APowerUp();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	USkeletalMeshComponent* GetPowerUpMesh() { return powerUpMesh; }

	UAudioComponent* PlayPowerUpSound(class USoundCue* Sound);

	void OnStartFire();
	void OnStopFire();

	class APlayerCharacter* GetMyOwner() { return MyOwner; }
	void SetMyOwner(class APlayerCharacter* ch) { MyOwner = ch; }

	void WeaponTrace();
	//void DoDamage();
	bool wasPickedUp() { return pickedUp; }
    
    void OnSpawnExpire();
    void SpawnNow();

protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = PowerUpMesh)
		USkeletalMeshComponent* powerUpMesh;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* FireLoopSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* FireFinishSound;

	UPROPERTY(Transient)
	class UAudioComponent* FireAC;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	class UParticleSystem* MuzzleFX;

	UPROPERTY(Transient)
	class UParticleSystemComponent* FireParticle;

	UPROPERTY(EditAnywhere)
		float FireRate;

	UPROPERTY(EditAnywhere)
		float WeaponRange;

	UPROPERTY(EditDefaultsOnly)
	class UParticleSystem* HitParticle;

	UPROPERTY(EditAnywhere, Category = Damage)
		float Damage = 2.0f;

	FTimerHandle WeaponTimer;

	class APlayerCharacter* MyOwner;

private:
	bool pickedUp;
};
