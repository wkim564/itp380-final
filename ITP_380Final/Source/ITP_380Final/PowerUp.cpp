// Fill out your copyright notice in the Description page of Project Settings.

#include "ITP_380Final.h"
#include "PowerUp.h"


// Sets default values
APowerUp::APowerUp()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    powerUpMesh =  CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PowerUpMesh"));
    RootComponent = powerUpMesh;
    powerUpMesh->SetVisibility(true);
    FireRate = 0.1f;
    WeaponRange = 0000.0f;
}

// Called when the game starts or when spawned
void APowerUp::BeginPlay()
{
	Super::BeginPlay();
    pickedUp = false;
    //this->SetActorLocation(FVector(-250.0f, 0.0f, 220.0f));

}

// Called every frame
void APowerUp::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void APowerUp::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void APowerUp::OnStartFire()
{
	if (FireAC != nullptr)
    {
        FireAC = PlayPowerUpSound(FireLoopSound);
    }
    FireParticle = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, powerUpMesh, TEXT("MuzzleFlashSocket"));

    
    GetWorldTimerManager().SetTimer(WeaponTimer, this, &APowerUp::WeaponTrace, FireRate, true);
}

void APowerUp::OnStopFire()
{
    if (FireAC != nullptr)
    {
        FireAC->Stop();
    }
    if (FireParticle != nullptr)
    {
        FireParticle->DeactivateSystem();
    }
    PlayPowerUpSound(FireFinishSound);
    
    GetWorldTimerManager().ClearTimer(WeaponTimer);
}

UAudioComponent* APowerUp::PlayPowerUpSound(USoundCue *Sound)
{
    UAudioComponent *AC = NULL;
    
    if (Sound)
    {
        AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
    }
    return AC;
}

void APowerUp::SpawnNow()
{
    printf("\n\nIn spawnnow\n\n");
    this->GetPowerUpMesh()->SetVisibility(true);
}

void APowerUp::OnSpawnExpire()
{
    printf("\n\nIn spawnexpire\n\n");
    this->GetPowerUpMesh()->SetVisibility(false);
}

void APowerUp::WeaponTrace()
{
   /* static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
    static FName MuzzleSocket = FName(TEXT("MuzzleFlashSocket"));
    // Start from the muzzle's position
    FVector StartPos = powerUpMesh->GetSocketLocation(MuzzleSocket);
    // Get forward vector of MyPawn
    FVector Forward = MyOwner->GetActorForwardVector();
    // Calculate end position
    FVector EndPos = StartPos + Forward*WeaponRange;
    // Perform trace to retrieve hit info
    FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
    TraceParams.bTraceAsyncScene = true;
    TraceParams.bReturnPhysicalMaterial = true;
    // This fires the ray and checks against all objects w/ collision
    FHitResult Hit(ForceInit);
    GetWorld()->LineTraceSingleByObjectType(Hit, StartPos, EndPos, FCollisionObjectQueryParams::AllObjects, TraceParams);
    // Did this hit anything?
    if (Hit.bBlockingHit)
    {
        UGameplayStatics::SpawnEmitterAtLocation(this, HitParticle, Hit.ImpactPoint);
        
        AEnemy* enem = Cast<AEnemy>(Hit.GetActor());
        if (enem)
        {
            enem->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
        }
    
    }*/
}
/*
void APowerUp::DoDamage()
{
    pickedUp = true;
    UGameplayStatics::GetPlayerPawn(this, 0)->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
}*/