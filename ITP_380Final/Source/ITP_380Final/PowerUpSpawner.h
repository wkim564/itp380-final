// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PowerUpSpawner.generated.h"

UCLASS()
class ITP_380FINAL_API APowerUpSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APowerUpSpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    UPROPERTY(EditAnywhere)
    TArray<class ATargetPoint*> SpawnPoints;
    
    UPROPERTY(EditAnywhere)
    TSubclassOf<ACharacter> CharacterClass;
    
    UPROPERTY(EditAnywhere)
    float SpawnExpire;
    
    void OnSpawnExpire();
    void SpawnNow();
    
    void PickUp();
	
private:
    ACharacter *currPowerUp;
};
