// Fill out your copyright notice in the Description page of Project Settings.

#include "ITP_380Final.h"
#include "PowerUpV2.h"


// Sets default values
APowerUpV2::APowerUpV2()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APowerUpV2::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APowerUpV2::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void APowerUpV2::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

