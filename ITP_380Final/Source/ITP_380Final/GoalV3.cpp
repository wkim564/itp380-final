// Fill out your copyright notice in the Description page of Project Settings.

#include "ITP_380Final.h"
#include "GoalV3.h"


// Sets default values
AGoalV3::AGoalV3()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGoalV3::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGoalV3::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AGoalV3::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

