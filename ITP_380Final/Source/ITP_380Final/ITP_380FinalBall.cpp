#include "ITP_380Final.h"
#include "ITP_380FinalBall.h"
#include "PowerUp.h"
#include "PowerUpSpawner.h"
#include "GoalV2.h"
#include "Rocket.h"
#include "MyPlayerController.h"
#include "ITP_380FinalGameMode.h"

AITP_380FinalBall::AITP_380FinalBall()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BallMesh(TEXT("/Game/Rolling/Meshes/BallMesh.BallMesh"));

	// Create mesh component for the ball
	Ball = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ball0"));
	Ball->SetStaticMesh(BallMesh.Object);
	Ball->BodyInstance.SetCollisionProfileName(UCollisionProfile::PhysicsActor_ProfileName);
	Ball->SetSimulatePhysics(true);
	Ball->SetAngularDamping(0.1f);
	Ball->SetLinearDamping(0.1f);
	Ball->BodyInstance.MassScale = 3.5f;
	Ball->BodyInstance.MaxAngularVelocity = 800.0f;
	Ball->SetNotifyRigidBodyCollision(true);
	RootComponent = Ball;

	CapsuleComponent->AttachTo(RootComponent);

	// Create a camera boom attached to the root (ball)
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm0"));
	SpringArm->AttachTo(RootComponent);
	SpringArm->bDoCollisionTest = false;
	SpringArm->bAbsoluteRotation = true; // Rotation of the ball should not affect rotation of boom
	SpringArm->RelativeRotation = FRotator(-45.f, 0.f, 0.f);
	SpringArm->TargetArmLength = 1000.f;
	SpringArm->bEnableCameraLag = false;
	SpringArm->CameraLagSpeed = 3.f;

	// Create a camera and attach to boom
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera0"));
	Camera->AttachTo(SpringArm, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false; // We don't want the controller rotating the camera
    
    /*sphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
    sphereCollision->InitSphereRadius(100.0f);
    sphereCollision->AttachTo(RootComponent);
    sphereCollision->OnComponentHit.AddDynamic(this, &AITP_380FinalBall::OnHit);*/
    
    //USphereComponent *sph =
    bCanBeDamaged = true;

	// Set up forces
	RollTorque = 2000000.0f;
	JumpImpulse = 350000.0f;
	bCanJump = false; // Start being able to jump

	MyRocket = nullptr;
    MyPowerUp = nullptr;
	hasPowerUp = true;
}

void AITP_380FinalBall::BeginPlay()
{
	Super::BeginPlay();
	//CapsuleComponent->GetRelativeTransform().SetTranslation(FVector(0, 0, 0));
	int32 id = ((AMyPlayerController*)(this->GetController()))->GetLocalPlayer()->GetControllerId();
	if (id == 0){
		AITP_380FinalGameMode* GameMode = Cast<AITP_380FinalGameMode>(UGameplayStatics::GetGameMode(this));
		GameMode->player1 = (APlayerController*)(this->GetController());
		this->SetActorLocation(FVector(-250.0f, -480.0f, 220.0f));
		rotation.Yaw = 90;
        printf("\n\nSpawned player 1 at (%f, %f, %f)\n\n", this->GetActorLocation().X, this->GetActorLocation().Y, this->GetActorLocation().Z);
	}
	else{
		this->SetActorLocation(FVector(-250.0f, 480.0f, 220.0f));
		rotation.Yaw = -90;
        printf("\n\nSpawned player 2 at (%f, %f, %f)\n\n", this->GetActorLocation().X, this->GetActorLocation().Y, this->GetActorLocation().Z);
	}
	CapsuleComponent->GetRelativeTransform().SetTranslation(FVector(0, 0, 0));
	// Spawn the weapon, if one was specified
    
    //MyPowerUp = GetWorld()->SpawnActor<APowerUp>(PowerUpClass);


	if (RocketClass)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// Need to set rotation like this because otherwise gun points down
			FRotator Rotation(0.0f, 0.0f, 0.0f);
			// Spawn the Weapon
			MyRocket = World->SpawnActor<ARocket>(RocketClass, FVector::ZeroVector, Rotation, SpawnParams);
			if (MyRocket)
			{
                // This is attached to "WeaponPoint" which is defined in the skeleton
				MyRocket->GetRocketMesh()->AttachTo(GetMesh(), TEXT("RocketSocket"));
                MyRocket->SetMyOwner(this);
			}
		}
	}
    
    //FTimerHandle fth;
    //GetWorldTimerManager().SetTimer(fth, this, &AITP_380FinalBall::LosePowerUp, 2.0f);
    LosePowerUp();
    //SetActorLocation(this->GetActorLocation(RootComponent), true);
}

void AITP_380FinalBall::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{


	// set up gameplay key bindings
	/*InputComponent->BindAxis("MoveRight", this, &AITP_380FinalBall::MoveRight);
	InputComponent->BindAxis("MoveForward", this, &AITP_380FinalBall::MoveForward);

	InputComponent->BindAction("Jump", IE_Pressed, this, &AITP_380FinalBall::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &AITP_380FinalBall::Float);*/

}

void AITP_380FinalBall::MoveForward(float Val)
{
	float multiplier = Val*RollTorque;
	const FVector Torque = Ball->GetForwardVector()*multiplier;
	Ball->AddForce(Torque);
}

void AITP_380FinalBall::MoveRight(float Val)
{
	rotation.Yaw += 2.0f*Val;
	auto forward = Ball->GetForwardVector();

	/*const FVector Torque = FVector(0.f, Val * RollTorque, 0.f);
	Ball->AddForce(Torque);*/
}

void AITP_380FinalBall::NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	//bCanJump = true;
    //printf("hit notified!!!\n\n\n\n\n\n");
    
	APowerUp* pu = Cast<APowerUp>(Other);
    if (pu)
    {
        MyPowerUp = pu;

        CollectPowerUp(MyPowerUp);
    }
    
    //APowerUp* pu2 = Cast<APowerUp>(OtherComp);
    //if (Other->IsA(AActor::StaticClass()))
    //{
    //    printf("Try 2 works\n\n\n\n");
    //}
}


void AITP_380FinalBall::OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{/*
    printf("hit registered!!!\n\n\n\n\n\n");
    ACharacter* pu = Cast<ACharacter>(OtherActor);
    if (pu)
    {
        AITP_380FinalBall* ba = Cast<AITP_380FinalBall>(OtherActor);
        if (ba)
        {
            printf("Collided with enemy\n\n\n\n");
        }
        else
        {
            printf("Collided with a powerup\n\n\n\n");

        }
    }
    else
    {
        
    }*/
}

void AITP_380FinalBall::OnOverlap(class AActor *OtherActor, class UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool FromSweep, const FHitResult &SweepResult)
{
    printf("Overlap registered!!!!\n\n\n\n");
}

void AITP_380FinalBall::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
	
	SetActorRotation(rotation);
    //RootComponent->SetWorldLocation(FVector(GetActorLocation().X, GetActorLocation().Y, 220.0f));
    SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, 220.0f));
	auto CameraRotation = rotation;
	CameraRotation.Pitch = -25.0f;
	SpringArm->SetRelativeRotation(CameraRotation);
    
}

void AITP_380FinalBall::Stop(){
	auto velocity = Ball->GetPhysicsLinearVelocity();

	velocity.Z = 0.0f;
	velocity.Normalize();
	FRotator fRotator = velocity.Rotation();

	Ball->SetPhysicsLinearVelocity(FVector(0, 0, 0));
}

void AITP_380FinalBall::OnStartFire()
{
    printf("\n\nAITP_380FinallBall:: On start fire\n\n");

	if (hasPowerUp)
    {
		MyRocket->OnStartFire();
	}
}

void AITP_380FinalBall::OnStopFire()
{
    printf("\n\nAITP_380FinalBall:: On stop fire\n\n");

	MyRocket->OnStopFire();
}

void AITP_380FinalBall::deacMesh()
{
	GetMesh()->Deactivate();
}

bool AITP_380FinalBall::isDead()
{
	if (bCanBeDamaged == false)
	{
		return true;
	}
	return false;
}

float AITP_380FinalBall::TakeDamage(float dam, const FDamageEvent &DamageEvent, AController *EventInstigator, AActor *DamageCauser)
{
    Super::TakeDamage(dam, DamageEvent, EventInstigator, DamageCauser);
    Stop();
	return 0.1f;
}

void AITP_380FinalBall::CollectPowerUp(APowerUp* DamageCauser)
{
	if (hasPowerUp == false)
	{
		hasPowerUp = true;
        Stop();
		MyRocket->GetRocketMesh()->SetVisibility(true);

        
        //MyPowerUp->OnSpawnExpire();
        printf("\n\nIn collect power up\n\n");
        //MyPowerUp = DamageCauser;
        if (MyPowerUp)
        {
            printf("\n\npower up isnt null\n\n");
            MyPowerUp->GetPowerUpMesh()->SetVisibility(false);
            MyPowerUp->SetActorEnableCollision(false);
        }
        //DamageCauser->GetMesh()->SetVisibility(false);

        FTimerHandle fth;
        GetWorldTimerManager().SetTimer(fth, this, &AITP_380FinalBall::LosePowerUp, 15.0f);
	}
}

void AITP_380FinalBall::LosePowerUp()
{
	if (hasPowerUp == true)
	{
		hasPowerUp = false;
        MyRocket->GetRocketMesh()->SetVisibility(false);
        printf("\n\n in lose power up\n\n");
        if (MyPowerUp)
        {
            printf("\n\n power up isnt null in lose\n\n");
            MyPowerUp->GetPowerUpMesh()->SetVisibility(true);
            MyPowerUp->SetActorEnableCollision(true);
        }
        //MyPowerUp->SpawnNow();

	}
    OnStopFire();
}

void AITP_380FinalBall::reset(){
	int32 id = ((AMyPlayerController*)(this->GetController()))->GetLocalPlayer()->GetControllerId();
	if (id == 0){
		this->SetActorLocation(FVector(-250.0f, -550.0f, 220.0f));
		rotation.Yaw = 90;
	}

	else{
		this->SetActorLocation(FVector(-250.0f, 550.0f, 220.0f));
		rotation.Yaw = -90;
	}
	Ball->SetPhysicsLinearVelocity(FVector(0, 0, 0));
}