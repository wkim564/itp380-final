// Fill out your copyright notice in the Description page of Project Settings.

#include "ITP_380Final.h"
#include "PowerUpSpawner.h"
#include "Engine/TargetPoint.h"
#include "PowerUp.h"

// Sets default values
APowerUpSpawner::APowerUpSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    SpawnExpire = 7.0f;
}

// Called when the game starts or when spawned
void APowerUpSpawner::BeginPlay()
{
	Super::BeginPlay();
    FTimerHandle fth;
    GetWorldTimerManager().SetTimer(fth, this, &APowerUpSpawner::SpawnNow, 10.0f,true);

}

// Called every frame
void APowerUpSpawner::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );

}


void APowerUpSpawner::OnSpawnExpire()
{
    printf("\n\n Trying to unspawn now\n\n");

    if (currPowerUp)
    {
        currPowerUp->GetMesh()->SetVisibility(false);
        currPowerUp->Destroy();
    }
}

void APowerUpSpawner::SpawnNow()
{
    printf("\n\n Trying to spawn now\n\n");
    int n = SpawnPoints.Num();
    if (n > 0)
    {
        int ind = FMath::RandRange(0, n-1);
        
        FVector loc = SpawnPoints[ind]->GetActorLocation();
        FRotator rot = SpawnPoints[ind]->GetActorRotation();
        
        currPowerUp = GetWorld()->SpawnActor<ACharacter>(CharacterClass, loc, rot);
        if (currPowerUp)
        {
            //currPowerUp->GetMesh()->SetVisibility(true);
            currPowerUp->SpawnDefaultController();
        }
        //currPowerUp->
        printf("spawned at loc %d", ind);
    }
    //FTimerHandle fth;
    //GetWorldTimerManager().SetTimer(fth, this, &APowerUpSpawner::OnSpawnExpire, 8.0f);
}