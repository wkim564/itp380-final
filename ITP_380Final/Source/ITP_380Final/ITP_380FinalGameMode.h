// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "ITP_380FinalGameMode.generated.h"

UCLASS(minimalapi)
class AITP_380FinalGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AITP_380FinalGameMode();
	
	void BeginPlay() override;
	void Timer();
	void WinScreen();
	UFUNCTION(BlueprintCallable, Category = Score) FString GetRedScore();
	UFUNCTION(BlueprintCallable, Category = Score) FString GetBlueScore();
	int RedScore;
	int BlueScore;
	UFUNCTION(BlueprintCallable, Category=Time) FString GetTime() { return FString(sMins + ":" + sSeconds); }

	void ResetWorld();

	UPROPERTY(EditAnywhere)
	class APlayerController* player1;

	UPROPERTY(EditAnywhere)
	class APlayerController* player2;

	UPROPERTY(EditAnywhere)
	class AActor* ball;
private:
	FTimerHandle handle;
	int WinTime;
	int time;
	int mins;
	int seconds;
	FString sMins;
	FString sSeconds;
	FString sRedScore;
	FString sBlueScore;

};



