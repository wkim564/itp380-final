// Fill out your copyright notice in the Description page of Project Settings.

#include "ITP_380Final.h"
#include "GoalV2.h"


// Sets default values
AGoalV2::AGoalV2()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGoalV2::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGoalV2::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

