// Fill out your copyright notice in the Description page of Project Settings.

#include "ITP_380Final.h"
#include "MyPlayerController.h"
#include "ITP_380FinalBall.h"


AMyPlayerController::AMyPlayerController(){

}

void AMyPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// set up gameplay key bindings
	int32 id = GetLocalPlayer()->GetControllerId();
	if (id == 0){
		InputComponent->BindAxis("MoveRight_P1", this, &AMyPlayerController::MoveRight);
		InputComponent->BindAxis("MoveForward_P1", this, &AMyPlayerController::MoveForward);

		InputComponent->BindAction("Stop_P1", IE_Pressed, this, &AMyPlayerController::Stop);
        InputComponent->BindAction("Shoot_P1", IE_Pressed, this, &AMyPlayerController::OnStartFire);
        InputComponent->BindAction("Shoot_P1", IE_Released, this, &AMyPlayerController::OnStopFire);


	}
	else if (id == 1){
		InputComponent->BindAxis("MoveRight_P2", this, &AMyPlayerController::MoveRight);
		InputComponent->BindAxis("MoveForward_P2", this, &AMyPlayerController::MoveForward);

        InputComponent->BindAction("Stop_P2", IE_Pressed, this, &AMyPlayerController::Stop);
		InputComponent->BindAction("Shoot_P2", IE_Pressed, this, &AMyPlayerController::OnStartFire);
        InputComponent->BindAction("Shoot_P2", IE_Released, this, &AMyPlayerController::OnStopFire);

	}
	bShowMouseCursor = true;
}

void AMyPlayerController::MoveRight(float Value){
	if (Value != 0.0f)
	{
		APawn* Pawn = GetPawn();
		if (Pawn)
		{
			Cast<AITP_380FinalBall>(Pawn)->MoveRight(Value);
		}
	}
}

void AMyPlayerController::MoveForward(float Value){
	if (Value != 0.0f)
	{
		APawn* Pawn = GetPawn();
		if (Pawn)
		{
			Cast<AITP_380FinalBall>(Pawn)->MoveForward(Value);
		}
	}
}

void AMyPlayerController::Stop(){
	APawn* Pawn = GetPawn();
	if (Pawn)
	{
		Cast<AITP_380FinalBall>(Pawn)->Stop();
	}
}

void AMyPlayerController::OnStopFire()
{
    printf("\n\n AMYPlayerController:: On stop fire\n\n");

    APawn* Pawn = GetPawn();
    if (Pawn)
    {
        Cast<AITP_380FinalBall>(Pawn)->OnStopFire();
    }
}

void AMyPlayerController::OnStartFire()
{
    printf("\n\nAMyPlayerController:: On start fire\n\n");

    APawn* Pawn = GetPawn();
    if (Pawn)
    {
        Cast<AITP_380FinalBall>(Pawn)->OnStartFire();
    }
}