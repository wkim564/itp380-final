// Fill out your copyright notice in the Description page of Project Settings.

#include "ITP_380Final.h"
#include "Rocket.h"
#include "ITP_380FinalBall.h"
#include "Particles/ParticleSystemComponent.h"
#include "Ball.h"

// Sets default values
ARocket::ARocket()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    RocketMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("RocketMesh"));
    RootComponent = RocketMesh;
    
    FireRate = 0.1f;
    WeaponRange = 10000.0f;
}

// Called when the game starts or when spawned
void ARocket::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARocket::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ARocket::OnStartFire()
{
    printf("\n\nARocket:: On start fire\n\n");

    //if (FireAC != nullptr)
    //{
        FireAC = PlayRocketSound(FireLoopSound);
    //}
   // if (FireParticle != nullptr)
    //{
        FireParticle = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, RocketMesh, TEXT("MuzzleFlashSocket"));
   // }
    //FireParticle = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, RocketMesh, TEXT("MuzzleFlashSocket"));
    
    GetWorldTimerManager().SetTimer(WeaponTimer, this, &ARocket::WeaponTrace, FireRate, true);
}

void ARocket::OnStopFire()
{
    printf("\n\nARocket:: On stop fire\n\n");
    if (FireAC != nullptr)
    {
        FireAC->Stop();
    }
    if (FireParticle != nullptr)
    {
        FireParticle->DeactivateSystem();
    }
    //PlayRocketSound(FireFinishSound);
    
    GetWorldTimerManager().ClearTimer(WeaponTimer);
}

UAudioComponent* ARocket::PlayRocketSound(USoundCue *Sound)
{
    UAudioComponent *AC = NULL;
    
    if (Sound)
    {
        AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
    }
    return AC;
}

void ARocket::WeaponTrace()
{
     static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
     static FName MuzzleSocket = FName(TEXT("MuzzleFlashSocket"));
     // Start from the muzzle's position
     FVector StartPos = RocketMesh->GetSocketLocation(MuzzleSocket);
     // Get forward vector of MyPawn
     FVector Forward = MyOwner->GetActorForwardVector();
     // Calculate end position
     FVector EndPos = StartPos + Forward*WeaponRange;
    
     // Perform trace to retrieve hit info
     FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
     TraceParams.bTraceAsyncScene = true;
     TraceParams.bReturnPhysicalMaterial = true;
     // This fires the ray and checks against all objects w/ collision
     FHitResult Hit(ForceInit);
     GetWorld()->LineTraceSingleByObjectType(Hit, StartPos, EndPos, FCollisionObjectQueryParams::AllObjects, TraceParams);
     // Did this hit anything?
     if (Hit.bBlockingHit)
     {
         UGameplayStatics::SpawnEmitterAtLocation(this, HitParticle, Hit.ImpactPoint);
		 if (Hit.GetActor()->IsA<ABall>()){
			 ABall* ball = (ABall*)(Hit.GetActor());
			 ball->Ball->AddForce(Forward * 2000000);
		 }
         AITP_380FinalBall* enem = Cast<AITP_380FinalBall>(Hit.GetActor());
         if (enem)
         {
             printf("\n\n SHOT ENEMY!!!!!!!!!!!!! \n\n");
            enem->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
         }
     
     }
}