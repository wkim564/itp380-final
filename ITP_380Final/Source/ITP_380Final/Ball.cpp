// Fill out your copyright notice in the Description page of Project Settings.

#include "ITP_380Final.h"
#include "Ball.h"
#include "Goal.h"
#include "ITP_380FinalBall.h"
#include "ITP_380FinalGameMode.h"


// Sets default values
ABall::ABall()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> BallMesh(TEXT("/Game/Rolling/Meshes/BallMesh.BallMesh"));

	// Create mesh component for the ball
	Ball = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ball0"));
	Ball->SetStaticMesh(BallMesh.Object);
	Ball->BodyInstance.SetCollisionProfileName(UCollisionProfile::PhysicsActor_ProfileName);
	Ball->SetSimulatePhysics(true);
	Ball->SetAngularDamping(0.1f);
	Ball->SetLinearDamping(0.1f);
	Ball->BodyInstance.MassScale = 3.5f;
	Ball->BodyInstance.MaxAngularVelocity = 800.0f;
	Ball->SetNotifyRigidBodyCollision(true);
	gameOver = false;
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	AITP_380FinalGameMode* GameMode = Cast<AITP_380FinalGameMode>(UGameplayStatics::GetGameMode(this));
	GameMode->ball = this;
	SetActorLocation(FVector(-250.0f, 0.0f, 220.0f));
	Ball->SetPhysicsLinearVelocity(FVector(0, 0, 0));
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABall::NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	//bCanJump = true;
	//printf("hit notified!!!\n\n\n\n\n\n");

	if (Other->IsA<AGoal>())
	{
		AGoal* goal = Cast<AGoal>(Other);
		AITP_380FinalGameMode* GameMode = Cast<AITP_380FinalGameMode>(UGameplayStatics::GetGameMode(this));
		if (!gameOver){
			if (goal->isRedGoal)
			{
				GameMode->RedScore++;

			}
			else
			{
				GameMode->BlueScore++;

			}
			GameMode->ResetWorld();
		}

	}

	//APowerUp* pu2 = Cast<APowerUp>(OtherComp);
	//if (Other->IsA(AActor::StaticClass()))
	//{
	//    printf("Try 2 works\n\n\n\n");
	//}
}